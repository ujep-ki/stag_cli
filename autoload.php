<?php
    include_once 'src/Classes/Data/ChoiceOption.php';
    include_once 'src/Classes/Data/SubjectData.php';

    include_once 'src/Classes/Command/FindRoomsCommand.php';
    include_once 'src/Classes/Command/FindSubjectsCommand.php';
    include_once 'src/Classes/Command/FindThesisCommand.php';

    include_once 'src/Tools/ApiClient.php';
    include_once 'src/Tools/Tools.php';