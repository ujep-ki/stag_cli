<?php
    include_once "vendor/autoload.php";
    include_once 'autoload.php';

    use Symfony\Component\Console\Application as ConsoleManipulator;
    use techniktomcz\stagcli\Classes\Command\FindRoomsCommand;
    use techniktomcz\stagcli\Classes\Command\FindSubjectsCommand;
    use techniktomcz\stagcli\Classes\Command\FindThesisCommand;
    use techniktomcz\stagcli\Tools\ApiClient;

    $apiClient = new ApiClient();

    $consoleManipulator = new ConsoleManipulator();
    $consoleManipulator->setName('Stag CLI Tool');

    $commands = [
        new FindSubjectsCommand(),
        new FindRoomsCommand(),
        new FindThesisCommand()
    ];

    foreach ($commands as $command) {
        $command->setApiClient($apiClient);
    }

    $consoleManipulator->addCommands($commands);

    try {
        $consoleManipulator->run();
    } catch (Exception $e) {
        var_dump($e);
    }