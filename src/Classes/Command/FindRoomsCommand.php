<?php
    namespace techniktomcz\stagcli\Classes\Command;

    use Exception;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use techniktomcz\stagcli\Tools\ApiClient;
    use techniktomcz\stagcli\Tools\Tools;

    class FindRoomsCommand extends Command {
        private ApiClient $apiClient;

        protected function configure()
        {
            $this->setDescription('Můžeš hledat místnosti podle Názvu mebo pracovišťe/katedry nebo zkratky nebo roku');
            $this->setName('find:rooms');
            $this->setAliases(['f:r']);
            $this->addUsage("--building <building shortcut>");
            $this->addUsage("-d <department shortcut>");

            $this->addOption('building', ['b'], InputArgument::OPTIONAL, 'Vyhledat podle Budovy');
            $this->addOption('room-number', ['rn'], InputArgument::OPTIONAL, 'Vyhledat podle Čísla učebny');
            $this->addOption('department', ['d'], InputArgument::OPTIONAL, 'Vyhledat podle Katedry/Pracoviště');
            $this->addOption('max-people', ['map'], InputArgument::OPTIONAL, 'Vyhledat podle maximální kapacity');
            $this->addOption('min-people', ['mip'], InputArgument::OPTIONAL, 'Vyhledat podle minimální kapacity');
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $parsedParams = $this->parseOptions($input->getOptions());

            try {
                $data = $this->apiClient->findRoomsByParams($parsedParams);

                $header = ['Katedra', 'Zkratka místnosti', 'Číslo dveří', 'Typ', 'Kapacita'];

                $parsedData = [];

                var_dump($parsedParams);

                foreach ($data as $room) {
                    $parsedData[] = [
                        $room['katedra'],
                        $room['zkrBudovy'] . $room['cisloMistnosti'],
                        $room['typCiselne'],
                        $room['typ'],
                        $room['kapacita']
                    ];
                }

                Tools::renderTable($output, $header, $parsedData);

                return Command::SUCCESS;
            } catch (Exception $e) {
                echo "Error ($e)";

                return Command::FAILURE;
            }


        }

        private function parseOptions(array $options) {
            $map = [
                'department' => 'pracoviste',
                'room-number' => 'cisloMistnosti',
                'min-people' => 'kapacitaOd',
                'max-people' => 'kapacitaDo',
                'building' => 'zkrBudovy'
            ];

            $data = [];

            foreach ($options as $optionKey => $optionValue) {
                if($optionValue != null && array_key_exists($optionKey, $map)) {
                    $data[$map[$optionKey]] = $optionValue;
                }
            }

            return $data;
        }

        /**
         * @param ApiClient $apiClient
         */
        public function setApiClient(ApiClient $apiClient): void
        {
            $this->apiClient = $apiClient;
        }
    }