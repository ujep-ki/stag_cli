<?php
    namespace techniktomcz\stagcli\Classes\Command;

    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Style\SymfonyStyle;
    use techniktomcz\stagcli\Classes\Data\ChoiceOption;
    use techniktomcz\stagcli\Classes\Data\SubjectData;
    use techniktomcz\stagcli\Tools\ApiClient;
    use techniktomcz\stagcli\Tools\Tools;

    class FindSubjectsCommand extends Command {
        private ApiClient $apiClient;

        protected function configure()
        {
            $this->setDescription('Můžeš hledat předměty podle Názvu mebo pracovišťe/katedry nebo zkratky nebo roku');
            $this->setName('find:subjects');
            $this->setAliases(['f:s']);
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $styler = new SymfonyStyle($input, $output);

            $options = [
                new ChoiceOption(1, 'Podle názvu'),
                new ChoiceOption(2, 'Podle pracovišťe/katedry'),
                new ChoiceOption(3, 'Podle zkratky'),
                new ChoiceOption(4, 'Podle roku')
            ];

            $selectedOption = ChoiceOption::parseChoice($styler->choice('Předměty se dají vyhledávat podle těchto způsobů', $options), $options);

            $styler = new SymfonyStyle($input, $output);

            if ($selectedOption !== -1) {
                switch ($selectedOption) {
                    case 1:
                        $name = $styler->ask('Zadej název');

                        $data = $this->apiClient->findSubjectByName($name);

                        Tools::renderTable(
                            $output,
                            SubjectData::ArrayHeader,
                            array_map('techniktomcz\stagcli\Classes\Data\SubjectData::ToArray', $data)
                        );
                        break;
                    case 2:
                        $options = ['KI', 'KMA', 'KFY', 'KBI', 'CJP'];

                        $name = $styler->choice('Vyberte zkratku katedry', $options, $options[0]);

                        $data = $this->apiClient->findSubjectByPlace($name);

                        Tools::renderTable(
                            $output,
                            SubjectData::ArrayHeader,
                            array_map('techniktomcz\stagcli\Classes\Data\SubjectData::ToArray', $data)
                        );
                        break;
                    case 3:
                        $shortcut = $styler->ask('Zadej zkratku předmětu');

                        $data = $this->apiClient->findSubjectByShortcut($shortcut);

                        Tools::renderTable(
                            $output,
                            SubjectData::ArrayHeader,
                            array_map('techniktomcz\stagcli\Classes\Data\SubjectData::ToArray', $data)
                        );
                        break;
                    case 4:
                        break;
                }

                return Command::SUCCESS;
            }

            $styler->writeln('Zadal jsi nemožnou odpověd!');
            return Command::FAILURE;
        }

        /**
         * @param ApiClient $apiClient
         */
        public function setApiClient(ApiClient $apiClient): void
        {
            $this->apiClient = $apiClient;
        }
    }