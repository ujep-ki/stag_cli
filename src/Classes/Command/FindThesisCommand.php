<?php
    namespace techniktomcz\stagcli\Classes\Command;

    use Exception;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Style\SymfonyStyle;
    use techniktomcz\stagcli\Tools\ApiClient;
    use techniktomcz\stagcli\Tools\Tools;

    class FindThesisCommand extends Command {
        private ApiClient $apiClient;

        protected function configure()
        {
            $this->addOption('assigned-at', [],InputArgument::OPTIONAL, 'Vyhledat podle roku zadání')
                ->addOption('defense-at', [],InputArgument::OPTIONAL, 'Vyhledat podle roku obhajoby');

            $this->setName('find:thesis');
            $this->setDescription('Hledání diplomovích prací');
            $this->setAliases(['f:t']);
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $styler = new SymfonyStyle($input, $output);


            $parsedOptions = $this->parseOptions($input->getOptions());

            $options = ['KI', 'KMA', 'KFY', 'KBI', 'CJP'];

            $department = $styler->choice('Vyberte zkratku katedry', $options, $options[0]);

            $parsedOptions['katedra'] = $department;

            try {
                $data = $this->apiClient->findThesis($parsedOptions);

                $header = ['Vedoucí', 'Student', 'Téma', 'Klíčová slova', 'Jazyk'];

                $parsedData = [];

                foreach ($data as $room) {
                    $parsedData[] = [
                        $room['vedouciJmeno'],
                        $room['student']['jmeno'] . ' ' . $room['student']['prijmeni'],
                        $room['temaHlavni'],
                        $room['klicSlova'],
                        $room['jazyk']
                    ];
                }

                Tools::renderTable($output, $header, $parsedData);

                return Command::SUCCESS;
            } catch (Exception $e) {
                echo "Error ($e)";
            }

            return Command::FAILURE;
        }

        private function parseOptions(array $arguments) {
            $map = [
                'assigned-at' => 'rokZadani',
                'defense-at' => 'rokObhajoby'
            ];

            $data = [];

            foreach ($arguments as $argumentKey => $argumentValue) {
                if($argumentValue != null && array_key_exists($argumentKey, $map)) {
                    $data[$map[$argumentKey]] = $argumentValue;
                }
            }

            return $data;
        }

        /**
         * @param ApiClient $apiClient
         */
        public function setApiClient(ApiClient $apiClient): void
        {
            $this->apiClient = $apiClient;
        }
    }