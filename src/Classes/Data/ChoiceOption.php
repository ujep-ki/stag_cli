<?php
    namespace techniktomcz\stagcli\Classes\Data;

    class ChoiceOption {
        private string $name;
        private int $id;

        /**
         * A constructor.
         * @param string $name
         * @param int $id
         */
        public function __construct(int $id, string $name)
        {
            $this->name = $name;
            $this->id = $id;
        }

        public function getId()
        {
            return $this->id;
        }

        public function __toString()
        {
            return $this->name;
        }

        public function equals(string $object) {
            return $this->name === $object;
        }

        public static function parseChoice($aName, array $aArray): int {
            foreach ($aArray as $element) {
                if($element->equals($aName)) {
                    return $element->getId();
                }
            }

            return -1;
        }
    }