<?php
    namespace techniktomcz\stagcli\Classes\Data;

    class SubjectData {
        public const ArrayHeader = ['Katedra', 'Zkratka', 'Název', 'Rok'];

        private string $katedra;
        private string $zkratka;
        private string $rok;
        private string $nazev;

        /**
         * SubjectData constructor.
         * @param $katedra
         * @param $zkratka
         * @param $rok
         * @param $nazev
         */
        public function __construct($katedra, $zkratka, $rok, $nazev)
        {
            $this->katedra = $katedra;
            $this->zkratka = $zkratka;
            $this->rok = $rok;
            $this->nazev = $nazev;
        }

        public static function ParseArray(array $array): SubjectData {
            return new SubjectData($array['katedra'], $array['zkratka'], $array['rok'], $array['nazev']);
        }

        public static function ToArray(SubjectData $data): array {
            return [$data->katedra, $data->zkratka, $data->nazev, $data->rok];
        }

        /**
         * @return string
         */
        public function getKatedra(): string
        {
            return $this->katedra;
        }

        /**
         * @return string
         */
        public function getZkratka(): string
        {
            return $this->zkratka;
        }

        /**
         * @return string
         */
        public function getRok(): string
        {
            return $this->rok;
        }

        /**
         * @return string
         */
        public function getNazev(): string
        {
            return $this->nazev;
        }


    }
