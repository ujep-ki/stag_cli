<?php
    namespace techniktomcz\stagcli\Tools;

    use Exception;
    use GuzzleHttp\Psr7\Request;
    use Psr\Http\Client\ClientExceptionInterface;
    use techniktomcz\stagcli\Classes\Data\SubjectData;
    use GuzzleHttp\Client as HttpClient;

    class ApiClient {
        private HttpClient $httpClient;
        private array $defaultHeaders;

        public function __construct($basePath = 'https://ws.ujep.cz/ws/services/rest2/') {
            $this->defaultHeaders = ['Accept' => 'application/json'];
            $this->httpClient = new HttpClient(['base_uri' => $basePath]);
        }

        public function findSubjectByName(string $subjectName) {
            $encodedName = urlencode("%$subjectName%");

            return $this->getSubjects("nazev=$encodedName");
        }

        public function findSubjectByShortcut(string $subjectShortcut) {
            $encodedSubjectShortcut = urlencode("%$subjectShortcut%");

            return $this->getSubjects("zkratka=$encodedSubjectShortcut");
        }

        public function findSubjectByPlace(string $workplaceShortcut) {
            $encodedWorkplaceShortcut = urlencode("%$workplaceShortcut%");

            return $this->getSubjects("pracoviste=$encodedWorkplaceShortcut");
        }

        private function getSubjects(string $requestParams) {
            $request = new Request('GET', "predmety/najdiPredmety?$requestParams", $this->defaultHeaders);
            try {
                $response = $this->httpClient->sendRequest($request);
            } catch (ClientExceptionInterface $e) {
                echo "Error in API client! ($e)";
                die;
            }

            if($response->getStatusCode() !== 200) {
                $responseCode = $response->getStatusCode();
                $responseContent = $response->getBody()->getContents();

                throw new Exception("ERROR ($responseCode): $responseContent");
            }

            $subjects = json_decode($response->getBody()->getContents(), true)['predmetKatedry'];

            $formattedData = [];

            foreach($subjects as $foundedSubject) {
                $formattedSubject = [];

                foreach ($foundedSubject as $key => $value) {
                    if($value != NULL) {
                        $formattedSubject[$key] = $value;
                    }
                }

                $formattedData[] = SubjectData::ParseArray($formattedSubject);
            }

            return $formattedData;
        }

        public function findRoomsByParams(array $parsedParams): array {
            $params = join('&', array_map(function ($key) use ($parsedParams) { $v = urlencode($parsedParams[$key]); $k = urlencode($key); return "$k=$v"; }, array_keys($parsedParams)));

            var_dump($params);

            $request = new Request('GET', "mistnost/getMistnostiInfo?$params", $this->defaultHeaders);
            $response = $this->httpClient->sendRequest($request);

            if($response->getStatusCode() !== 200) {
                $responseCode = $response->getStatusCode();
                $responseContent = $response->getBody()->getContents();

                throw new Exception("ERROR ($responseCode): $responseContent");
            }

            return json_decode($response->getBody()->getContents(), true)['mistnostInfo'];
        }

        public function findThesis(array $parsedOptions) {
            $stringedOptions = join('&', array_map(function ($key) use ($parsedOptions) { $v = urlencode($parsedOptions[$key]); $k = urlencode($key); return "$k=$v"; }, array_keys($parsedOptions)));

            $request = new Request('GET', "kvalifikacniprace/getKvalifikacniPrace?$stringedOptions", $this->defaultHeaders);
            $response = $this->httpClient->sendRequest($request);

            if($response->getStatusCode() !== 200) {
                $responseCode = $response->getStatusCode();
                $responseContent = $response->getBody()->getContents();

                throw new Exception("ERROR ($responseCode): $responseContent");
            }

            return json_decode($response->getBody()->getContents(), true)['kvalifikacniPrace'];
        }
    }
